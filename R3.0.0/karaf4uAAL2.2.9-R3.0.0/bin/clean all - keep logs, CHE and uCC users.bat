@echo "Cleaning Karaf caches"
rmdir /s /q ..\data\cache\
rmdir /s /q ..\data\tmp\
rmdir /s /q ..\data\derby\

@echo "Cleaning uAAL leftover file"
rmdir /s /q ..\uCC\tempUsrvFiles\
del /q ..\etc\ui.dm\persistency\*.dlg
del /q ..\etc\ui.dm\persistency\*.msg
del /q ..\instances\*.properties

@echo "Cleaning installed application and Deploy Manager registries"
del /q ..\deploy\*.kar
del /q ..\deploy\*.jar
del /q ..\uAAL\deploy\*.registry
del /q ..\uAAL\tmp\*.*
del /q ..\etc\mw.managers.deploy\*.*
del /q ..\etc\uAAL\*.registry
rmdir /s /q ..\etc\tmp\
del /q ..\services.xml

@echo "Cleaining service logs"
del /q ..\WorkingMemory.log
del /q ..\Agenda.log
del /q ..\lock
rmdir /s /q ..\etc\shopping\