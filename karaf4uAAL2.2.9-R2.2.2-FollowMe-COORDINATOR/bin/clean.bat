@echo "Cleaning Karaf caches"
rmdir /s /q ..\data\cache\
rmdir /s /q ..\data\tmp\
rmdir /s /q ..\data\derby\

@echo "Cleaning uAAL leftover file"
rmdir /s /q ..\uCC\user\
rmdir /s /q ..\uCC\tempUsrvFiles\
rmdir /s /q ..\etc\ctxt.che\store\
del /q ..\etc\ui.dm\persistency\*.dlg
del /q ..\etc\ui.dm\persistency\*.msg

@echo "Cleaning installed application and Deploy Manager registries"
del /q ..\deploy\*.kar
del /q ..\deploy\*.jar
del /q ..\uAAL\deploy\*.registry
del /q ..\etc\mw.managers.deploy\*.registry

